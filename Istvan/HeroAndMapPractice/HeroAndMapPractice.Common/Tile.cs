﻿namespace HeroAndMapPractice.Common
{
    public enum Tile
    {
        Desert = 0,
        Oasis = 1,
        Mountain = 2,
        Water = 3
    };
}
