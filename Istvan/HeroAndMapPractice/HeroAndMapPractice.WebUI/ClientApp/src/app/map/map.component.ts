import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  mapWidth: number = 5;
  mapHeight: number = 4;

  constructor() { }

  ngOnInit(): void {
  }

}
