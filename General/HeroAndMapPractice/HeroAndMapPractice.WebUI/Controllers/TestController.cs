﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace HeroAndMapPractice.Controllers
{
    public class TestController : Controller
    {
        private static readonly string[] _testList = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public IEnumerable<string> Get()
        {
            return _testList;
        }
    }
}
