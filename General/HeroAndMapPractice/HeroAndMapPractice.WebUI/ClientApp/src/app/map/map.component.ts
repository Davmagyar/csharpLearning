import { Component, Input, OnInit } from '@angular/core';
import { Hero } from '../models/hero';
import { MapTile } from '../models/map-tile';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  hero: Hero;
  map: MapTile[][];
  mapTileEnum = MapTile;
  mapTileSizeInPx: number = 100;
  mapTileBorderInPx: number = 10;

  constructor() {
    this.map = [
      [ MapTile.Desert, MapTile.Water, MapTile.Desert, MapTile.Desert, MapTile.Desert ],
      [ MapTile.Oasis, MapTile.Desert, MapTile.Desert, MapTile.Desert, MapTile.Desert ],
      [ MapTile.Desert, MapTile.Desert, MapTile.Water, MapTile.Desert, MapTile.Desert ],
      [ MapTile.Oasis, MapTile.Desert, MapTile.Desert, MapTile.Desert, MapTile.Desert ],
      [ MapTile.Desert, MapTile.Desert, MapTile.Mountain, MapTile.Mountain, MapTile.Desert ]
    ];

    this.hero = new Hero(1, 2, 3);
  }

  ngOnInit(): void {
    
  }

}
