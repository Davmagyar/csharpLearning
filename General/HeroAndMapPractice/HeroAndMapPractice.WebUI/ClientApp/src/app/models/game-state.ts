import { Hero } from "./hero";
import { MapTile } from "./map-tile";

export class GameState {
    constructor(public heroes: Hero[], public map: MapTile[][]) {}
}