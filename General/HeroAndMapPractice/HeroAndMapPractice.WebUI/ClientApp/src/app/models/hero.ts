export class Hero {
    constructor(
        public id: number,
        public posRow: number,
        public posCol: number) {}
}