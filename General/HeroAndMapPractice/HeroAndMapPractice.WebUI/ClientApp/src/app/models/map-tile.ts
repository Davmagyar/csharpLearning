export enum MapTile {
    Desert = 1,
    Mountain,
    Oasis,
    Water
}