import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Direction } from './models/direction';
import { GameState } from './models/game-state';
import { Hero } from './models/hero';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  currentGameState: GameState = new GameState([], [[]]);

  MoveHero (heroId: number, direction: Direction): void {
    const currentHero = this.currentGameState.heroes.find(hero => hero.id === heroId);
    if (!currentHero) {
      throw "Hero id not found!";
    }
    
    switch (direction)  {
      case Direction.Up:
        ++currentHero.posRow;
        break;
      case Direction.Right:
        ++currentHero.posCol;
        break;
      case Direction.Down:
        --currentHero.posRow;
        break;
      case Direction.Left:
        --currentHero.posCol;
        break;
    }
  }

  constructor() { }
}
