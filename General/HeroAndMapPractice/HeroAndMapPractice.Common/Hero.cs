﻿namespace HeroAndMapPractice.Common
{
    public class Hero
    {
        public int Id { get;  set; }
        public string Name { get; set; }
        public int PositionX { get; set; }
        public int PositionY { get; set; }
    }
}
