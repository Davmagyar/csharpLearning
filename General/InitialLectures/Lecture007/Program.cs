﻿using System;
namespace Lectures.Lecture007
{

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var x = new MyClass();
            throw new InvalidOperationException();
            x.Dispose();
            //változtatás master
            //változtatás dev

            try
            {
                using (var y = new MyClass())
                {
                    Console.WriteLine("Hello World!");
                    throw new InvalidOperationException();
                    Console.WriteLine("Hello World 2!");
                }
            }
            catch
            {
                Console.WriteLine("Hello World 3!");
            }
        }

        public class MyClass : IDisposable
        {
            public void Dispose()
            {
                Console.WriteLine("Dispose!");
            }
        }

    }
}
