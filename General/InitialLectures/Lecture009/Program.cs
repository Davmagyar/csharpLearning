﻿using System;

namespace Lecture.Lecture009
{
    class Program
    {
        static void Main(string[] args)
        {
            Base x = new Dervied();
            x.DoSth();
            String y;
            
        }
    }

    /// <summary>
    /// Ez az osztály egy minta ősosztály.
    /// </summary>
    class Base
    {
        /// <summary>
        /// Kiír egy üzenetet.
        /// </summary>
        public void DoSth()
        {
            Console.WriteLine("Base");
        }
    }

    class Dervied : Base
    {
        public new void DoSth()
        {
            Console.WriteLine("Derived");
        }
    }

//Webes felületek(userek hívogatják a böngészőből)

//CRUD = Create, Read, Update, Delete
// - Listázni(get)
// - Részletek megtekintése(get)
// - Létrehozás(post)
// - Update(post)
// - Törlés(post)

//GET kérés -> nem változtat a szerver és az adatbázis állapotán, nem hoz létre vagy módosít adatot + HTML-el térhet visssza
//POST kérés -> elindít egy kérést a szerver felé, amely adatok megváltozásával járhat.Ha sikerrel jár, akkor egy Redirect metódus követheti, ami egy GET kérés segítségével átvisz egy másik oldalra

//Példa a szerkesztésre:
//GET: MyWebsite/Details? id = 1(1 - es azonosítójú elemet megtekinteném)
//GET: MyWebsite/Edit? id = 1(1 - es azonosítójú elemet szerkesztem)
//POST: MyWebsite/Edit? id = 1(elmenteném a módosításokat, hogy megmaradjanak)
//GET: MyWebsite/Details? id = 1 (redirection)

//Példa a törlésre:
//GET: MyWebsite/List(elemek listáját megtekintem)
//POST: MyWebsite/DeleteFromList
//GET: 

//Webszolgálatások(API, programok hívogatják)

//Webszolgáltatások esetén:
//(PUT kérés: update, DELETE kérés: törlés)

}
