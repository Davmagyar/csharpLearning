﻿using Lectures.CsharpLearning.XmlExample;
using System;
using System.IO;
using System.Xml.Serialization;

namespace Lectures.CsharpLearning
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var x = new Fruit
                {
                    AgeInDays = 1,
                    Color = "Red",
                    Name = "Dinnye",
                    //Text = "random"
                };

                Garden y = null;
                Console.WriteLine(y?.FruitCount());

                var xmlSerializer = new XmlSerializer(typeof(Garden));
                using (var streamReader = new StreamReader("./XMLFile1.xml"))
                {
                    var result = xmlSerializer.Deserialize(streamReader) as Garden;
                    throw new MyException("Valami hiba");
                }
            }
            catch (MyException me)
            {
                Console.WriteLine("Üzleti hiba érkezett: " + me.Message);
            }            
        }



    }


}
