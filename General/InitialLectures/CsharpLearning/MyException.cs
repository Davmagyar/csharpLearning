﻿using System;

namespace Lectures.CsharpLearning
{
    public class MyException : Exception
    {
        public MyException(string message) : base(message) { }
    }
}
