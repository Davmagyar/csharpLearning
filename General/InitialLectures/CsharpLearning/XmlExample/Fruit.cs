﻿using System.Xml.Serialization;

namespace Lectures.CsharpLearning.XmlExample
{
	[XmlRoot(ElementName = "fruit")]
	public class Fruit
	{

        [XmlElement(ElementName = "color")]
        public string Color { get; set; }

        [XmlElement(ElementName = "ageInDays")]
        public int AgeInDays { get; set; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        //[XmlText]
        //public string Text { get; set; }
    }
}



