﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Lectures.CsharpLearning.XmlExample
{
    [XmlRoot(ElementName = "garden")]
	public class Garden
	{

		[XmlElement(ElementName = "fruit")]
		public List<Fruit> Fruit { get; set; }

		public string LookAround()
        {
			return "Gyümölcsök száma: " + Fruit.Count;
        }

		public int FruitCount()
		{
			return Fruit.Count;
		}
	}
}
